
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import java.awt.Dimension;

public class AddingStudent extends JFrame {

	static AddingStudent frame;
	private JPanel panel;

	private JTextField textField;
	private JTextField nameField;
	private JTextField surnameField;
	private JTextField emailField;
	private JTextField courseField;
	private JTextField telephoneNrField;
	private JTextField paymentField;
	private JTextField addressField;
	private JTextField ageField;
	private JTextField payedField;
	private JTextField feeField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new AddingStudent();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	


	/**
	 * Create the application.
	 */
	public AddingStudent() {

		setBounds(600, 600, 950, 600);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		panel.setSize(new Dimension(1020, 1020));
		setContentPane(panel);
		panel.setBackground(SystemColor.activeCaption);
		panel.setForeground(Color.WHITE);

		JLabel lblNewLabel_1 = new JLabel("Adding Student");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 20));

		JLabel lblNewLabel_12 = new JLabel("Name");
		lblNewLabel_12.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_13 = new JLabel("Surname");
		lblNewLabel_13.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_14 = new JLabel("Email");
		lblNewLabel_14.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel = new JLabel("Course");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_2 = new JLabel("TelephoneNr");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_3 = new JLabel("Payment");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_4 = new JLabel("Address");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_5 = new JLabel("Age");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_6 = new JLabel("Payed");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_7 = new JLabel("Fee");
		lblNewLabel_7.setFont(new Font("Tahoma", Font.PLAIN, 15));

		nameField = new JTextField();
		nameField.setColumns(10);

		surnameField = new JTextField();
		surnameField.setColumns(10);

		emailField = new JTextField();
		emailField.setColumns(10);

		courseField = new JTextField();
		courseField.setColumns(10);

		telephoneNrField = new JTextField();
		telephoneNrField.setColumns(10);

		paymentField = new JTextField();
		paymentField.setColumns(10);

		addressField = new JTextField();
		addressField.setColumns(10);

		JButton addButton = new JButton("Add");
		addButton.setVerticalAlignment(SwingConstants.BOTTOM);
		addButton.setFont(new Font("Tahoma", Font.PLAIN, 15));

		
		addButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				int status1 = 0;
				int payment = Integer.parseInt(paymentField.getText());
				int fee = Integer.parseInt(feeField.getText());

				if ((paymentField.getText().equals("")) || (payedField.getText().equals(""))
						|| (feeField.getText().equals(""))) {
					JOptionPane.showMessageDialog(frame, "Please fill fields payment");

				} else if (payment< fee) {

					JOptionPane.showMessageDialog(frame, " Incorrect fee must be less than payment");

				} else {
					String name = nameField.getText();
					String surname = surnameField.getText();
					String email = emailField.getText();
					String course = courseField.getText();
					String telephoneNr = telephoneNrField.getText();
					payment = Integer.parseInt(paymentField.getText());
					String address = addressField.getText();
					int age = Integer.parseInt(ageField.getText());
					int payed = Integer.parseInt(payedField.getText());
					fee = Integer.parseInt(feeField.getText());

					Student student = new Student(name, surname, email, course, telephoneNr, payment, address, age,
							payed, fee);

					status1 = StudentDAO.save(student);

					if (status1 > 0) {

						JOptionPane.showMessageDialog(frame, "you have succesfully inserted a new record");

						nameField.setText("");
						surnameField.setText("");
						emailField.setText("");
						courseField.setText("");
						telephoneNrField.setText("");
						paymentField.setText("");
						addressField.setText("");
						ageField.setText("");
						payedField.setText("");
						feeField.setText("");

					} else {
						JOptionPane.showMessageDialog(frame, "unable to add a  record");
					}

				}

			}

		});

		JButton backButton = new JButton("Back");
		backButton.setFont(new Font("Tahoma", Font.PLAIN, 15));

		backButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				SecretarySection.main(new String[] {});

			}

		});

		ageField = new JTextField();
		ageField.setColumns(10);

		payedField = new JTextField();
		payedField.setColumns(10);

		feeField = new JTextField();
		feeField.setColumns(10);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup()
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel.createSequentialGroup()
						.addContainerGap(249, Short.MAX_VALUE)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(Alignment.TRAILING, gl_panel
								.createSequentialGroup()
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup().addGroup(gl_panel
												.createParallelGroup(Alignment.LEADING)
												.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup().addGroup(
														gl_panel.createParallelGroup(Alignment.LEADING).addGroup(
																Alignment.TRAILING,
																gl_panel.createSequentialGroup().addGroup(gl_panel
																		.createParallelGroup(Alignment.LEADING)
																		.addGroup(Alignment.TRAILING, gl_panel
																				.createSequentialGroup()
																				.addComponent(addButton,
																						GroupLayout.PREFERRED_SIZE, 67,
																						GroupLayout.PREFERRED_SIZE)
																				.addGap(24))
																		.addGroup(Alignment.TRAILING,
																				gl_panel.createSequentialGroup()
																						.addComponent(lblNewLabel_12)
																						.addGap(105)))
																		.addPreferredGap(ComponentPlacement.RELATED))
																.addGroup(Alignment.TRAILING, gl_panel
																		.createSequentialGroup()
																		.addGroup(gl_panel
																				.createParallelGroup(Alignment.LEADING)
																				.addComponent(lblNewLabel_13)
																				.addComponent(lblNewLabel_2))
																		.addGap(71)))
														.addPreferredGap(ComponentPlacement.RELATED))
												.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
														.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
																.addComponent(lblNewLabel).addComponent(lblNewLabel_14))
														.addGap(92)))
												.addPreferredGap(ComponentPlacement.RELATED))
										.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
												.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
														.addComponent(lblNewLabel_4).addComponent(lblNewLabel_3))
												.addGap(88)))
								.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(Alignment.TRAILING,
										gl_panel.createSequentialGroup()
												.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
														.addComponent(lblNewLabel_6).addComponent(lblNewLabel_5)
														.addComponent(lblNewLabel_7))
												.addGap(86)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addComponent(backButton, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
								.addComponent(nameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(surnameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(emailField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(courseField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(telephoneNrField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(paymentField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(addressField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(ageField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(payedField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(feeField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panel.createSequentialGroup().addGap(395).addComponent(lblNewLabel_1)))
				.addContainerGap(403, Short.MAX_VALUE)));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup().addGap(29).addComponent(lblNewLabel_1).addGap(3)
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel.createSequentialGroup()
						.addGap(33)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(nameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_12))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(surnameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_13))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(emailField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_14))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(courseField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(telephoneNrField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_2))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(paymentField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_3))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(addressField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_4))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(ageField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_5))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(payedField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_6))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(feeField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_7))
						.addContainerGap(115, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.RELATED, 425, Short.MAX_VALUE)
								.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(backButton)
										.addComponent(addButton))
								.addGap(19)))));
		panel.setLayout(gl_panel);

	}
}
