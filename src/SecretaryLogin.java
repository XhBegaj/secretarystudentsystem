import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;

import java.awt.Font;
import java.awt.SystemColor;

public class SecretaryLogin extends JFrame {

	static SecretaryLogin frame;
	private JPanel panel;
	private JTextField usernameField;
	private JTextField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new SecretaryLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SecretaryLogin() {

		setBounds(200, 200, 850, 500);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		setContentPane(panel);
		panel.setBackground(SystemColor.activeCaption);
		panel.setForeground(Color.WHITE);

		JLabel lblNewLabel = new JLabel("Secretary");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));

		JLabel lblNewLabel_1 = new JLabel("Username");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_2 = new JLabel("Password");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 15));

		usernameField = new JTextField();
		usernameField.setColumns(10);

		JPasswordField passwordField = new JPasswordField();
		passwordField.setColumns(10);

		JButton loginButton = new JButton("Login");
		loginButton.setFont(new Font("Tahoma", Font.PLAIN, 15));

		loginButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				String name = usernameField.getText();

				String password = String.valueOf(passwordField.getPassword());

				if (name.equals("") || password.equals("")) {
					JOptionPane.showMessageDialog(frame, "Enter username and password");
				} else {
					boolean status = SecretaryDAO.validate(name, password);
					if (status) {

						JOptionPane.showMessageDialog(frame, "You are sucessfully logined");

						SecretarySection.main(new String[] {});
						frame.dispose();
					} else {
						JOptionPane.showMessageDialog(frame, "Invalid username or password");
						usernameField.setText("");
						passwordField.setText("");
					}

				}
			}
		});

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING,
						gl_panel.createSequentialGroup().addContainerGap(393, Short.MAX_VALUE).addComponent(lblNewLabel)
								.addGap(385))
				.addGroup(gl_panel.createSequentialGroup().addGap(286)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING).addComponent(lblNewLabel_1)
								.addComponent(lblNewLabel_2))
						.addGap(27)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false).addComponent(usernameField)
								.addComponent(passwordField).addComponent(loginButton, GroupLayout.DEFAULT_SIZE,
										GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addContainerGap(365, Short.MAX_VALUE)));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup().addGap(29).addComponent(lblNewLabel).addGap(36)
				.addGroup(gl_panel
						.createParallelGroup(Alignment.BASELINE).addComponent(lblNewLabel_1).addComponent(usernameField,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGap(61)
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblNewLabel_2).addComponent(
						passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE))
				.addGap(79).addComponent(loginButton).addContainerGap(162, Short.MAX_VALUE)));
		panel.setLayout(gl_panel);

	}

}
