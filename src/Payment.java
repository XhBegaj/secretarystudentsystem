
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.SystemColor;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

public class Payment extends JFrame {

	static Payment frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new Payment();
					frame.setBounds(100, 100, 850, 400);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Payment() {

		List<Student> list = StudentDAO.getPayment();
		int size = list.size();

		String data[][] = new String[size][7];

		int row = 0;
		for (Student a : list) {

			data[row][0] = String.valueOf(a.getrollNumber());
			data[row][1] = a.getName();
			data[row][2] = a.getSurname();
			data[row][3] = a.getCourse();
			data[row][4] = a.getTelephoneNr();
			data[row][5] = String.valueOf(a.getPayment());
			data[row][6] = String.valueOf(a.getFee());
			

			row++;
		}

		String columnNames[] = { "id", "name", "surname", "Course", "ContactNumber", "Payment", "Fee" };

		JTable table = new JTable(data, columnNames);

		JScrollPane scroll = new JScrollPane(table);
		add(scroll);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}

}
