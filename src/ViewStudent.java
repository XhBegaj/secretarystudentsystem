
import java.awt.EventQueue;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class ViewStudent extends JFrame {

	static ViewStudent frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new ViewStudent();
					frame.setBounds(100, 100, 850, 400);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ViewStudent() {
		List<Student> list = StudentDAO.view();
		int size = list.size();

		String data[][] = new String[size][11];

		int row = 0;
		for (Student a : list) {
			data[row][0] = String.valueOf(a.getrollNumber());
			data[row][1] = a.getName();
			data[row][2] = a.getSurname();
			data[row][3] = a.getEmail();
			data[row][4] = a.getCourse();
			data[row][5] = a.getTelephoneNr();
			data[row][6] = String.valueOf(a.getPayment());
			data[row][7] = a.getAddress();
			data[row][8] = String.valueOf(a.getAge());
			data[row][9] = String.valueOf(a.getPayed());
			data[row][10] = String.valueOf(a.getFee());
			row++;
		}

		String columnNames[] = { "RollNumber", "Name", "Surname", "Email", "Course", "TelephoneNr", "Payment",
				"Address", "Age", "Payed", "Fee" };

		JTable table = new JTable(data, columnNames);

		JScrollPane scroll = new JScrollPane(table);
		add(scroll);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}
}