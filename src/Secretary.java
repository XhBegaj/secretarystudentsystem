

public class Secretary {

	private int id;
	private String name;
	private String surname;
	private String password;
	private String contactNumber;
	private String email;
	
	
	public Secretary() {
		
		
	}

public Secretary(String name,String surname,String password,String contactNumber,String email) {
		
		this.name=name;
		this.surname=surname;
		this.password=password;
		this.contactNumber=contactNumber;
		this.email=email;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getContactNumber() {
		return contactNumber;
	}


	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
	
	
}
