
public class Student {

	private String name, surname, email, course, telephoneNr, address;
	private int rollNumber, payment, age, payed, fee;

	public Student(int rollNumber, String name, String surname, String email, String course, String telephoneNr,
			int payment, String address, int age, int payed, int fee) {
		this.rollNumber = rollNumber;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.course = course;
		this.telephoneNr = telephoneNr;
		this.payment = payment;
		this.address = address;
		this.age = age;
		this.payed = payed;
		this.fee = fee;

	}

	public Student(String name, String surname, String email, String course, String telephoneNr, int payment,
			String address, int age, int payed, int fee) {
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.course = course;
		this.telephoneNr = telephoneNr;
		this.payment = payment;
		this.address = address;
		this.age = age;
		this.payed = payed;
		this.fee = fee;
	}

	public Student() {
		// TODO Auto-generated constructor stub
	}

	public int getrollNumber() {
		return rollNumber;

	}

	public void setrollNumber(int rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getTelephoneNr() {
		return telephoneNr;
	}

	public void setTelephoneNr(String telephoneNr) {
		this.telephoneNr = telephoneNr;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPayment() {
		return payment;
	}

	public void setPayment(int payment) {
		this.payment = payment;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getPayed() {
		return payed;
	}

	public void setPayed(int payed) {
		this.payed = payed;
	}

	public int getFee() {
		return fee;
	}

	public void setFee(int fee) {
		this.fee = fee;
	}

}