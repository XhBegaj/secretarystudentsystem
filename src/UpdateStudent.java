
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;

public class UpdateStudent extends JFrame {

	protected static final String Id = null;
	static UpdateStudent frame;
	private JPanel panel;
	private JTextField IDField;
	private JTextField NameField;
	private JTextField SurnameField;
	private JTextField EmailField;
	private JTextField CourseField;
	private JTextField TelephoneNumberField;
	private JTextField PaymentField;
	private JTextField AddressField;
	private JTextField AgeField;
	private JTextField PayedField;
	private JTextField FeeField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new UpdateStudent();

					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */

	public UpdateStudent() {

		setBounds(200, 200, 950, 600);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		setContentPane(panel);
		panel.setBackground(SystemColor.activeCaption);
		panel.setForeground(Color.WHITE);

		JLabel lblNewLabel = new JLabel("Update Student");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));

		JLabel lblNewLabel_1 = new JLabel("ID");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));

		IDField = new JTextField();
		IDField.setColumns(10);

		JButton btnNewButton = new JButton("Load");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 12));

		btnNewButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				String id = IDField.getText();
				if (id == null || id.trim().equals("")) {
					JOptionPane.showMessageDialog(frame, "Please enter ID");

				} else {
					int ID = Integer.parseInt(id);
					Student student1 = StudentDAO.getStudent(ID);

					NameField.setText(student1.getName());
					SurnameField.setText(student1.getSurname());
					EmailField.setText(student1.getEmail());
					CourseField.setText(student1.getCourse());
					TelephoneNumberField.setText(student1.getTelephoneNr());

					PaymentField.setText(String.valueOf(student1.getPayment()));
					AddressField.setText(student1.getAddress());
					AgeField.setText(String.valueOf(student1.getAge()));
					PayedField.setText(String.valueOf(student1.getPayment()));
					FeeField.setText(String.valueOf(student1.getFee()));

				}

			}
		});

		JLabel lblNewLabel_2 = new JLabel("Name");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 12));

		NameField = new JTextField();
		NameField.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("Surname");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 12));

		SurnameField = new JTextField();
		SurnameField.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("Email");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 12));

		EmailField = new JTextField();
		EmailField.setColumns(10);

		JLabel lblNewLabel_5 = new JLabel("Course");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 12));

		CourseField = new JTextField();
		CourseField.setColumns(10);

		JLabel lblNewLabel_6 = new JLabel("TelephoneNr");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 12));

		TelephoneNumberField = new JTextField();
		TelephoneNumberField.setColumns(10);

		JLabel lblNewLabel_7 = new JLabel("Payment");
		lblNewLabel_7.setFont(new Font("Tahoma", Font.PLAIN, 12));

		PaymentField = new JTextField();
		PaymentField.setColumns(10);

		JLabel lblNewLabel_8 = new JLabel("Address");
		lblNewLabel_8.setFont(new Font("Tahoma", Font.PLAIN, 12));

		AddressField = new JTextField();
		AddressField.setColumns(10);

		JLabel lblNewLabel_9 = new JLabel("Payed");
		lblNewLabel_9.setFont(new Font("Tahoma", Font.PLAIN, 12));

		AgeField = new JTextField();
		AgeField.setColumns(10);

		PayedField = new JTextField();
		PayedField.setColumns(10);

		FeeField = new JTextField();
		FeeField.setColumns(10);

		JLabel lblNewLabel_10 = new JLabel("Age");
		lblNewLabel_10.setFont(new Font("Tahoma", Font.PLAIN, 12));

		JButton btnNewButton_1 = new JButton("Update");
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 12));

		btnNewButton_1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				int status = 0;

				if ((PaymentField.getText().equals("")) || (PayedField.getText().equals(""))
						|| (FeeField.getText().equals(""))) {
					JOptionPane.showMessageDialog(frame, "unable to update a  record");
				} else {
					int rollnumber = Integer.parseInt(IDField.getText());
					String name = NameField.getText();
					String surname = SurnameField.getText();
					String email = EmailField.getText();
					String course = CourseField.getText();
					String telephoneNr = TelephoneNumberField.getText();
					int payment = Integer.parseInt(PaymentField.getText());
					String address = AddressField.getText();
					int age = Integer.parseInt(AgeField.getText());
					int payed = Integer.parseInt(PayedField.getText());
					int fee = Integer.parseInt(FeeField.getText());

					Student student1 = new Student(rollnumber, name, surname, email, course, telephoneNr, payment,
							address, age, payed, fee);

					status = StudentDAO.update(student1);

					if (status > 0) {

						JOptionPane.showMessageDialog(frame, "you have succesfully updated a  record");
						IDField.setText("");
						NameField.setText("");
						SurnameField.setText("");
						EmailField.setText("");
						CourseField.setText("");
						TelephoneNumberField.setText("");
						PaymentField.setText("");
						AddressField.setText("");
						AgeField.setText("");
						PayedField.setText("");
						FeeField.setText("");

					} else {
						JOptionPane.showMessageDialog(frame, "unable to update a  record");
					}

				}

			}

		});

		JLabel lblNewLabel_11 = new JLabel("Fee");
		lblNewLabel_11.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup()
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel.createSequentialGroup()
						.addContainerGap(346, Short.MAX_VALUE)
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
								.addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel.createSequentialGroup().addGap(11)
														.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
																.addComponent(lblNewLabel_3).addComponent(lblNewLabel_4)
																.addComponent(lblNewLabel_5)))
												.addComponent(lblNewLabel_6)
												.addGroup(gl_panel.createSequentialGroup().addGap(24)
														.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
																.addComponent(lblNewLabel_9)
																.addComponent(lblNewLabel_10)
																.addComponent(lblNewLabel_11))))
										.addGap(49))
								.addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addComponent(lblNewLabel_1).addComponent(lblNewLabel_2))
										.addGap(73))))
						.addGroup(
								Alignment.TRAILING,
								gl_panel.createSequentialGroup().addGap(356)
										.addGroup(gl_panel
												.createParallelGroup(Alignment.TRAILING).addComponent(lblNewLabel_7)
												.addComponent(lblNewLabel_8))
										.addGap(62)))
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(PaymentField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(IDField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel.createSequentialGroup()
								.addComponent(NameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGap(46).addComponent(btnNewButton))
						.addComponent(SurnameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(EmailField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(CourseField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(TelephoneNumberField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(AddressField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(AgeField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(PayedField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(FeeField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE))
				.addContainerGap(247, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING,
						gl_panel.createSequentialGroup().addContainerGap(421, Short.MAX_VALUE).addComponent(lblNewLabel)
								.addGap(374))
				.addGroup(gl_panel.createSequentialGroup().addGap(423).addComponent(btnNewButton_1).addContainerGap(444,
						Short.MAX_VALUE)));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup().addComponent(lblNewLabel)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel
								.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createSequentialGroup().addGap(29)
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(IDField, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblNewLabel_1)))
								.addGroup(gl_panel.createSequentialGroup().addGap(72).addGroup(gl_panel
										.createParallelGroup(Alignment.BASELINE).addComponent(btnNewButton)
										.addComponent(NameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(lblNewLabel_2))))
						.addGap(29)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(SurnameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_3))
						.addGap(26)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblNewLabel_4)
								.addComponent(EmailField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addGap(27)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(CourseField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_5))
						.addGap(39)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblNewLabel_6)
								.addComponent(TelephoneNumberField, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(28)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(PaymentField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_7))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(AddressField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_8))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(AgeField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_10))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(PayedField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_9))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(FeeField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_11))
						.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(btnNewButton_1)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		panel.setLayout(gl_panel);

	}
}
