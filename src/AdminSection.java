import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


import java.awt.Color;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.SystemColor;


public class AdminSection extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static AdminSection frame;
	private JPanel panel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new AdminSection();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdminSection() {

		setBounds(200, 200, 850, 500);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		setContentPane(panel);
		panel.setBackground(SystemColor.activeCaption);
		panel.setForeground(Color.WHITE);

		JLabel lblAdminSection = new JLabel("Admin Section");
		lblAdminSection.setFont(new Font("Tahoma", Font.PLAIN, 20));

		JButton addSecretary = new JButton("Add Secretary");
		addSecretary.setFont(new Font("Tahoma", Font.PLAIN, 15));
		addSecretary.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				AddingSecretary.main(new String[] {});

			}
		});

		JButton viewButton = new JButton("View Secretary");
		viewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		viewButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				ViewSecretary.main(new String[] {});

			}

		});

		JButton logoutButton = new JButton("Logout Admin");
		logoutButton.setFont(new Font("Tahoma", Font.PLAIN, 15));

		logoutButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				// TODO why you check if is enabled ?

				CourseSystem.main(new String[] {});

			}
		});

		GroupLayout groupLayout = new GroupLayout(panel);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(Alignment.TRAILING,
				groupLayout.createSequentialGroup().addContainerGap(359, Short.MAX_VALUE).addGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addComponent(lblAdminSection, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(logoutButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
										GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(viewButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
										GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(addSecretary, Alignment.LEADING)))
						.addGap(329)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addGap(21).addComponent(lblAdminSection).addGap(63)
						.addComponent(addSecretary).addGap(30).addComponent(viewButton).addGap(39)
						.addComponent(logoutButton).addContainerGap(206, Short.MAX_VALUE)));
		panel.setLayout(groupLayout);

	}

}
