
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.Color;

public class CourseSystem extends JFrame {

	static CourseSystem frame;
	private JPanel panel;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new CourseSystem();

					frame.setVisible(true);
				} catch (Exception e) {

					e.printStackTrace();

				}
			}
		});
	}

	/**
	 * Create the application.
	 */

	public CourseSystem() {
		setBounds(200, 200, 850, 500);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		setContentPane(panel);
		panel.setBackground(SystemColor.activeCaption);
		panel.setForeground(Color.WHITE);

		JLabel lblNewLabel = new JLabel("Course System");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);

		JButton adminButton = new JButton("Enter as Admin");
		adminButton.setFont(new Font("Tahoma", Font.PLAIN, 15));

		adminButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				AdminLogin.main(new String[] {});

			}
		});

		JButton secretaryButton = new JButton("Enter as Secretary");
		secretaryButton.setFont(new Font("Tahoma", Font.PLAIN, 15));

		secretaryButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				SecretaryLogin.main(new String[] {});

			}

		});

		GroupLayout groupLayout = new GroupLayout(panel);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap(319, Short.MAX_VALUE)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(Alignment.TRAILING,
												groupLayout.createSequentialGroup()
														.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 153,
																GroupLayout.PREFERRED_SIZE)
														.addGap(333))
										.addGroup(Alignment.TRAILING,
												groupLayout.createSequentialGroup()
														.addComponent(adminButton, GroupLayout.PREFERRED_SIZE, 201,
																GroupLayout.PREFERRED_SIZE)
														.addGap(306)))
								.addGroup(Alignment.TRAILING,
										groupLayout
												.createSequentialGroup().addComponent(secretaryButton,
														GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
												.addGap(314)))));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addGap(30).addComponent(lblNewLabel).addGap(71)
						.addComponent(adminButton, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
						.addGap(55)
						.addComponent(secretaryButton, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(194, Short.MAX_VALUE)));
		panel.setLayout(groupLayout);

	}

}
