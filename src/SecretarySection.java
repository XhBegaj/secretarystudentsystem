
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

// add pay button to secreatry section
// new class create table to  view the students that havent payed 

public class SecretarySection extends JFrame {

	static SecretarySection frame;
	private JPanel panel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new SecretarySection();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SecretarySection() {

		setBounds(200, 200, 850, 500);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		setContentPane(panel);
		panel.setBackground(SystemColor.activeCaption);
		panel.setForeground(Color.WHITE);

		JLabel lblNewLabel = new JLabel("Secretary Section");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));

		JButton btnAddStudent = new JButton("AddStudent");
		btnAddStudent.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnAddStudent.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				AddingStudent.main(new String[] {});

			}

		});

		JButton btnViewStudent = new JButton("View Student");
		btnViewStudent.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnViewStudent.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				ViewStudent.main(new String[] {});

			}

		});

		JButton btnEditStudent = new JButton("EditStudent");
		btnEditStudent.setFont(new Font("Tahoma", Font.PLAIN, 15));

		btnEditStudent.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				UpdateStudent.main(new String[] {});

			}

		});
		JButton btnSecretaryLogout = new JButton("Logout");
		btnSecretaryLogout.setFont(new Font("Tahoma", Font.PLAIN, 15));

		btnSecretaryLogout.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				CourseSystem.main(new String[] {});

			}

		});

		JButton btnPayment = new JButton("Payment");
		btnPayment.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnPayment.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				Payment.main(new String[] {});

			}

		});

		GroupLayout groupLayout = new GroupLayout(panel);
		groupLayout
				.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup().addGap(232)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(btnAddStudent).addComponent(btnEditStudent))
												.addGap(159)
												.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
														.addComponent(btnSecretaryLogout, GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addComponent(btnViewStudent, GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
										.addGroup(groupLayout.createSequentialGroup().addGap(345)
												.addComponent(lblNewLabel))
										.addGroup(
												groupLayout.createSequentialGroup().addGap(367).addComponent(btnPayment,
														GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)))
								.addContainerGap(219, Short.MAX_VALUE)));
		groupLayout
				.setVerticalGroup(
						groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(
										groupLayout.createSequentialGroup().addGap(41).addComponent(lblNewLabel)
												.addGap(54)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(btnAddStudent).addComponent(btnViewStudent))
												.addGap(34)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(btnEditStudent).addComponent(btnSecretaryLogout))
												.addGap(31).addComponent(btnPayment)
												.addContainerGap(201, Short.MAX_VALUE)));
		panel.setLayout(groupLayout);

	}
}
