
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import java.awt.SystemColor;
import java.awt.Window;

public class AddingSecretary extends JFrame {

	static AddingSecretary frame;
	private JPanel panel;
	private JTextField nameField;
	private JTextField surnameField;
	private JTextField passwordField;
	private JTextField contactNumberField;
	private JTextField emailField;
	private JButton addSecretaryButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new AddingSecretary();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddingSecretary() {

		setBounds(200, 200, 850, 500);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		setContentPane(panel);
		panel.setBackground(SystemColor.activeCaption);
		panel.setForeground(Color.WHITE);

		JLabel lblNewLabel = new JLabel("Adding Secretary");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));

		JLabel lblNewLabel_2 = new JLabel("Name");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_3 = new JLabel("Surname");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_4 = new JLabel("Password");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_5 = new JLabel("ContactNumber");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel_6 = new JLabel("Email");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 15));

		nameField = new JTextField();
		nameField.setColumns(10);

		surnameField = new JTextField();
		surnameField.setColumns(10);

		passwordField = new JTextField();
		passwordField.setColumns(10);

		contactNumberField = new JTextField();
		contactNumberField.setColumns(10);

		emailField = new JTextField();
		emailField.setColumns(10);

		addSecretaryButton = new JButton("Login");
		addSecretaryButton.setFont(new Font("Tahoma", Font.PLAIN, 15));

		addSecretaryButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				String name = nameField.getText();
				String surname = surnameField.getText();
				String password = passwordField.getText();
				String contactNumber = contactNumberField.getText();
				String email = emailField.getText();

				Secretary secretary = new Secretary(name, surname, password, contactNumber, email);

				int status = SecretaryDAO.save(secretary);

				if (status > 0) {

					JOptionPane.showMessageDialog(frame, "you have succesfully inserted a new record");

					nameField.setText("");
					surnameField.setText("");
					passwordField.setText("");
					contactNumberField.setText("");
					emailField.setText("");
				} else {
					JOptionPane.showMessageDialog(frame, "unable to add a  record");
				}

			}

		});

		JButton btnBack = new JButton("Back");
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 15));

		btnBack.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				AdminSection.main(new String[] {});

			}

		});

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup().addGap(340).addComponent(lblNewLabel).addContainerGap(343,
						Short.MAX_VALUE))
				.addGroup(gl_panel.createSequentialGroup().addGap(249)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING).addComponent(lblNewLabel_4)
								.addComponent(lblNewLabel_5).addComponent(lblNewLabel_6).addComponent(lblNewLabel_3)
								.addComponent(lblNewLabel_2))
						.addGap(48)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(emailField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(contactNumberField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(passwordField, Alignment.LEADING)
										.addComponent(surnameField, Alignment.LEADING)
										.addComponent(nameField, Alignment.LEADING)))
						.addGap(349))
				.addGroup(Alignment.LEADING,
						gl_panel.createSequentialGroup().addGap(320).addComponent(addSecretaryButton).addGap(81)
								.addComponent(btnBack, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(297, Short.MAX_VALUE)));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup().addContainerGap().addComponent(lblNewLabel).addGap(98)
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(nameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_2))
				.addGap(27)
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(surnameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_3))
				.addGap(18)
				.addGroup(gl_panel
						.createParallelGroup(Alignment.BASELINE).addComponent(lblNewLabel_4).addComponent(passwordField,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGap(18)
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup().addComponent(lblNewLabel_5).addGap(18)
								.addComponent(lblNewLabel_6))
						.addGroup(gl_panel.createSequentialGroup()
								.addComponent(contactNumberField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGap(18).addComponent(emailField, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
				.addPreferredGap(ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(addSecretaryButton)
						.addComponent(btnBack, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
				.addGap(57)));
		panel.setLayout(gl_panel);

	}
}
