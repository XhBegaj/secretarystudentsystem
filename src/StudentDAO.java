import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO {

	// jdbc driver name and database url

	static final String JDBC_Driver = " com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/course";

	// database credentials

	static final String USER = "root";
	static final String PASS = "user";

	public static Connection getCon() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost/course", USER, "");
		} catch (Exception e) {
			System.out.println(e);
		}

		return con;

	}

	public static int save(Student a) {

		int status1 = 0;

		try {

			Connection connection = getCon();

			PreparedStatement st = connection.prepareStatement(
					"INSERT INTO student (Name,Surname,Email,Course,TelephoneNr,Payment,Address,Age,Payed,Fee) VALUES (?,?,?,?,?,?,?,?,?,?)");

			st.setString(1, a.getName());
			st.setString(2, a.getSurname());
			st.setString(3, a.getEmail());
			st.setString(4, a.getCourse());
			st.setString(5, a.getTelephoneNr());
			st.setInt(6, a.getPayment());
			st.setString(7, a.getAddress());
			st.setInt(8, a.getAge());
			st.setInt(9, a.getPayed());
			st.setInt(10, a.getFee());

			status1 = st.executeUpdate();
			connection.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		return status1;

	}

	public static List<Student> view() {

		List<Student> student = new ArrayList<>();
		try {

			Connection conn = getCon();
			PreparedStatement ps = conn.prepareStatement("select * from student");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Student obj = new Student();

				obj.setrollNumber(rs.getInt(1));
				obj.setName(rs.getString(2));
				obj.setSurname(rs.getString(3));
				obj.setEmail(rs.getString(4));
				obj.setCourse(rs.getString(5));
				obj.setTelephoneNr(rs.getString(6));
				obj.setPayment(rs.getInt(7));
				obj.setAddress(rs.getString(8));
				obj.setAge(rs.getInt(9));
				obj.setPayed(rs.getInt(10));
				obj.setFee(rs.getInt(11));

				student.add(obj);
			}

			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		return student;

	}

	public static Student getStudent(int id) {

		Student student = new Student();
		student.setrollNumber(id);

		try {

			Connection conn = getCon();
			PreparedStatement ps = conn.prepareStatement("select * from student where Rollnumber= " + id);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				student.setrollNumber(rs.getInt(1));
				student.setName(rs.getString(2));
				student.setSurname(rs.getString(3));
				student.setEmail(rs.getString(4));
				student.setCourse(rs.getString(5));
				student.setTelephoneNr(rs.getString(6));
				student.setPayment(rs.getInt(7));
				student.setAddress(rs.getString(8));
				student.setAge(rs.getInt(9));
				student.setPayed(rs.getInt(10));
				student.setFee(rs.getInt(11));

			}

			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		return student;

	}

	public static int update(Student a) {

		int status = 0;

		try {

			Connection connection = getCon();

			PreparedStatement st = connection.prepareStatement(

					"UPDATE student SET Name=?,Surname=?,Email=?,Course=?,TelephoneNr=?,Payment=?,Address=?,Age=?,Payed=?,Fee=? where rollnumber=?");

			st.setString(1, a.getName());
			st.setString(2, a.getSurname());
			st.setString(3, a.getEmail());
			st.setString(4, a.getCourse());
			st.setString(5, a.getTelephoneNr());
			st.setInt(6, a.getPayment());
			st.setString(7, a.getAddress());
			st.setInt(8, a.getAge());
			st.setInt(9, a.getPayed());
			st.setInt(10, a.getFee());
			st.setInt(11, a.getrollNumber());

			status = st.executeUpdate();
			connection.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		return status;

	}

	public static List<Student> getPayment() {

		List<Student> student = new ArrayList<>();
		try {

			Connection conn = getCon();
			PreparedStatement ps = conn.prepareStatement(
					"select Rollnumber,Name,Surname,Course,TelephoneNr,Payment,Fee from student where Fee>0");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Student obj = new Student();

				obj.setrollNumber(rs.getInt(1));
				obj.setName(rs.getString(2));
				obj.setSurname(rs.getString(3));
				obj.setCourse(rs.getString(4));
				obj.setTelephoneNr(rs.getString(5));
				obj.setPayment(rs.getInt(6));
				obj.setFee(rs.getInt(7));

				student.add(obj);
			}

			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		return student;

	}

}