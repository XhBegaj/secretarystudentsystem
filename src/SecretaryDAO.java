import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SecretaryDAO {

	// jdbc driver name and database url

	static final String JDBC_Driver = " com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/course";

	// database credentials

	static final String USER = "root";
	static final String PASS = "user";

	public static Connection getCon() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost/course", USER, "");
		} catch (Exception e) {
			System.out.println(e);
		}

		return con;

	}

	public static int save(Secretary a) {

		int status = 0;

		try {

			Connection connection = getCon();

			PreparedStatement st = connection.prepareStatement(
					"INSERT INTO secretary (Name,Surname,Password,ContactNumber,email) VALUES (?,?,?,?,?)");

			st.setString(1, a.getName());
			st.setString(2, a.getSurname());
			st.setString(3, a.getPassword());
			st.setString(4, a.getContactNumber());
			st.setString(5, a.getEmail());

			status = st.executeUpdate();
			connection.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		return status;

	}

	public static boolean validate(String name, String password) {

		boolean status = false;

		try {
			Connection connection = getCon();
			PreparedStatement st = connection.prepareStatement("Select * from secretary where Name=? and Password=?");
			st.setString(1, name);
			st.setString(2, password);

			ResultSet rs = st.executeQuery();
			status = rs.next();
			connection.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		return status;
	}

	public static List<Secretary> view() {

		List<Secretary> secretary = new ArrayList<>();
		try {

			Connection conn = getCon();
			PreparedStatement ps = conn.prepareStatement("select * from secretary");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Secretary obj = new Secretary();

				obj.setId(rs.getInt(1));
				obj.setName(rs.getString(2));
				obj.setSurname(rs.getString(3));
				obj.setPassword(rs.getString(4));
				obj.setContactNumber(rs.getString(5));
				obj.setEmail(rs.getString(6));

				secretary.add(obj);
			}

			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		return secretary;

	}

}
