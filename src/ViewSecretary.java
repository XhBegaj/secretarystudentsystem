
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import sun.security.pkcs11.Secmod.DbMode;

import javax.swing.JTable;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

public class ViewSecretary extends JFrame {

	static ViewSecretary frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new ViewSecretary();
					frame.setBounds(100, 100, 850, 400);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ViewSecretary() {
		List<Secretary> list = SecretaryDAO.view();
		int size = list.size();

		String data[][] = new String[size][6];

		int row = 0;
		for (Secretary a : list) {
			data[row][0] = String.valueOf(a.getId());
			data[row][1] = a.getName();
			data[row][2] = a.getSurname();
			data[row][3] = a.getPassword();
			data[row][4] = a.getContactNumber();
			data[row][5] = a.getEmail();

			row++;
		}

		String columnNames[] = { "id", "name", "surname", "password", "ContactNumber", "Email" };

		JTable table = new JTable(data, columnNames);

		JScrollPane scroll = new JScrollPane(table);
		add(scroll);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);


	}
}